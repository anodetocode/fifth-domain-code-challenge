import { searchHero } from './search';
import { migrate } from '../database';

describe('Search', () => {
  describe('when hero exists', () => {
    it('returns an array of results', async () => {
      await migrate();
      const name = 'batman';
      const results = await searchHero(name);
      expect(results).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            id: expect.any(String),
            name: expect.any(String)
          })
        ])
      );
    });
  });

  describe(`when hero doesn't exist`, () => {
    it('returns an empty array', async () => {
      const name = 'not a hero';

      await expect(await searchHero(name)).toHaveLength(0);
    });
  });

  describe('when given invalid access token', () => {
    it('throws a specific error', async () => {
      const name = 'literally anything';
      const token = 'invalid';

      await expect(async () => await searchHero(name, token)).rejects.toThrow('access denied');
    });
  });
});
