import axios from 'axios';
import { getHeroStats } from '../database/hero';

const apiUrl = 'https://www.superheroapi.com/api.php';

const filterHero = hero => ({
  id: hero.id,
  name: hero.name,
  powerstats: hero.powerstats,
  image: hero.image.url
})

export async function searchHero(name, token=process.env.SUPERHERO_API_TOKEN) {
  try {
    const apiResponse = await axios.get(`${apiUrl}/${token}/search/${name}`);
    const data = apiResponse?.data;

    if (data?.response.includes('success')) {
      const apiResults = data.results.map(filterHero);
      const dbPromises = apiResults.map(async hero => await getHeroStats(hero));
      return await Promise.all(dbPromises);
    };

    if (data?.error.includes('not found')) {
      return [];
    }

    throw new Error(data?.error);
  } catch (error) {
    throw new Error('access denied');
  }
};
