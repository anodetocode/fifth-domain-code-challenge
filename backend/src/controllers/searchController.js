import { Router } from 'express';
import { searchHero } from '../services/search';
import { body, validationResult, param } from 'express-validator';

const router = Router();

router.get('/:name',
  param('name').exists().isString(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const name = req.params.name;
      await search(name, res)
  }
);

router.post('/',
  body('name').exists().isString(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const name = req.body.name;
    await search(name, res)
  }
);

async function search(name, res) {
  let results = [];

  try {
    results = await searchHero(name);
  } catch (error) {
    if (error.message.includes('access denied')) {
      return res.staus(403).send('Invalid token supplied.');
    }
    const msg = 'Error searching hero';
    console.log(`${msg}: `, error);
    return res.status(500).send(msg);
  }

  return res.status(200).send({ results });
};

export { router as searchController };