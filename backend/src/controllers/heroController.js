import { Router } from 'express';
import { saveHeroStats, getSavedHeroes } from '../database/hero';
import { body, validationResult, param } from 'express-validator';

const router = Router();

router.put('/:id',
  param('id').exists().isInt({ min: 1, max: 731}),
  body('name').exists().isString(),
  body('image').isString(),
  body('powerstats').exists().isObject(),
  body('powerstats.intelligence').exists().isString(),
  body('powerstats.strength').exists().isString(),
  body('powerstats.speed').exists().isString(),
  body('powerstats.durability').exists().isString(),
  body('powerstats.power').exists().isString(),
  body('powerstats.combat').exists().isString(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { id } = req.params;
    const { name , powerstats, image } = req.body;
    if ( id )
    try {
      await saveHeroStats(id, name, powerstats, image);
      res.status(204).end();
    } catch (error) {
      const msg = 'Error saving hero';
      console.log(`${msg}: `, error);
      res.status(500).send(msg);
    }
  }
);

router.get('/', async (req, res) => {
  try {
    const heroes = await getSavedHeroes();
    res.json(heroes);
  } catch (error) {
    const msg = 'Error retrieving saved heroes';
    console.log(`${msg}: `, error);
    res.status(500).send(msg);
  }
});

export { router as heroController }