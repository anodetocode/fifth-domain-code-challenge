import { database } from '.';

export async function saveHeroStats(id, name, powerstats, image) {
  try {
    await database.query(`
      INSERT INTO herotable (id, name, powerstats, image)
      VALUES ($1, $2, $3, $4)
      ON CONFLICT (id)
      DO UPDATE SET
      powerstats = excluded.powerstats,
      name = excluded.name,
      image = excluded.image
    `, [id, name, powerstats, image]);
  } catch (error) {
    console.log('Error saving hero stats: ', error);
    throw error;
  }
}

export async function getSavedHeroes() {
  try {
    const queryResult = await database.query('SELECT * FROM herotable');
    return { results: (queryResult?.rows || []) }
  } catch (error) {
    console.log('Error getting heroes: ', error);
    throw error;
  }
}

export async function getHeroStats(hero) {
  try {
    const queryResult = await database.query('SELECT * FROM herotable WHERE id = $1', [hero.id]);
    const result = queryResult?.rows[0] || hero;
    return result;
  } catch (error) {
    console.log('Error getting hero: ', error);
    throw error;
  }
}
