import { Pool, Client } from 'pg';

const pool = new Pool();

const migrate = async () => {
  console.log('Migrating database');

  const db = new Client();
  try {
    db.connect();
    await db.query(`
      DROP TABLE IF EXISTS herotable;
      CREATE TABLE herotable (
        id INT PRIMARY KEY,
        name VARCHAR(100),
        powerstats JSON,
        image VARCHAR(255)
      )
    `);
    db.end();
  } catch(error) {
    console.log('Error migrating database: ', error);
  }

}

export { pool as database, migrate };