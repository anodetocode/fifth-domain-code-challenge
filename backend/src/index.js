import express from 'express';
import { router } from './router';
import cors from 'cors';
import dotenv from 'dotenv';
import { migrate } from './database'

dotenv.config();

migrate();

const port = process.env.API_PORT || 3000;
const app = express();

app.use(cors());
app.use(express.json());
app.use(router);

app.listen(port, () => {
  console.log(`Listening on http://localhost:${port}`);
  console.log(`Try http://localhost:${port}/search/batman`);
});
