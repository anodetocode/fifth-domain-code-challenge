import express, { Router } from 'express';
import { searchController } from './controllers/searchController.js'
import { heroController } from './controllers/heroController.js'
import path from 'path';

const router = Router();

router.use('/search', searchController);
router.use('/heroes', heroController);
router.use('/', express.static(path.join(__dirname, 'api-doc')));

export { router };


