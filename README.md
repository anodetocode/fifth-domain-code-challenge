# Hero APP
## Original task text

We would like you to create a basic application for a superhero enthusiast, that can perform following functions:

1. Allow a user to search for their favourite superhero.
1. Select the superhero and see his/her image and power stats.
1. Edit the power stats
1. Save the image and stats to be viewed later.

So, essentially the app would allow a user to search superheroes by name, see his/her details, edit them and view all their saved super heroes.

You can use https://superheroapi.com/ for fetching super hero details

Our preferable stack is ReactJS, GraphQL, NodeJS and PostgreSQL, but feel free to use a different stack until you use NodeJS for back-end. Also, if you have time constraints and can only submit just back-end code, please let me know beforehand.

Please document all the assumptions you made and technical challenges you faced when doing this task.

Also add a Readme to allow us to run the app locally (Or you can also deploy it, and make things easier for us, but that is not a mandatory requirement)

## Challenge Response
### Dependencies
1. Node 14.15.X
1. minimum Docker 19
1. minimum `docker-compose` 1.26

### Quickstart (using Docker)
1. Copy .env.example to .env
1. Optionally set VPS_PUBLIC_API to your hostname, it is set to localhost by default
1. Change API URL in ./frontend/app/services/api-client.js to your VPS_PUBLIC_API (see enhancements)
1. From root directory run docker-compose
```bash
docker-compose up --build -d
```

---
### Run without Docker
#### Set up postgres
1. Install postgres
1. Set up postgres running on port *5432* (default port)
1. Log into postgres admin account
    ```bash
    psql postgres
    ```
1. Add new role *me*
    ```postgres
    postgres=# CREATE ROLE me WITH LOGIN PASSWORD 'password';
    postgres=# ALTER ROLE me CREATEDB;
    postgres=# \q
    ```
1. Log back in with new role
    ```bash
    psql -d postgres -U me
    ```
1. Create database for backend
    ```postgres
    postgres=> CREATE DATABASE heroapi;
    ```

#### Run the backend
```bash
cd backend
npm i
npm run start
```

#### Run the frontend
```bash
cd frontend
npm i
npm run start
```
---
### Enhancements
1. Full test coverage of business logic. Better and more granular unit tests.
1. Icon on frontend to show heroes that have been edited.
1. Delete saved heroes.
1. Fix frontend API to accept backend api url from an environment variable
