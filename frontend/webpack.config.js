const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');
const Dotenv = require('dotenv-webpack');

module.exports = {
  entry: "./app/index.js",
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: "bundle.js"
  },
  module: {
    rules: [
      { test: /\.jsx?$/, use: 'babel-loader' },
      { test: /\.css$/, use: [ 'style-loader', 'css-loader' ] }
    ]
  },
  mode: 'development',
  plugins: [
    new htmlWebpackPlugin({
      template: './app/index.html',
      favicon: './app/favicon.png'
    }),
    new Dotenv()
  ],
  resolve: {
    extensions: [ '.js', '.jsx']
  }
}
