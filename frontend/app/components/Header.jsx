import React from 'react';
import { searchHero, getSavedHeroes } from '../services/api-client';

export default class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    }
  }

  handleChange(e) {
    this.setState({ text: e.target.value });
  }

  search() {
    if (!this.state.text) return [];
    searchHero(this.state.text).then(( results ) => {
      if (!results) return;
      this.props.onChangeSearch(true);
      this.props.onChangeHeroes(results.results);
      this.setState({text: ''});
    })
    .catch(err => console.error('Search error: ', err));
  }

  getSaved() {
    getSavedHeroes().then(({ results }) => {
      this.props.onChangeSearch(false);
      this.props.onChangeHeroes(results);
      this.setState({text: ''});
    })
    .catch(err => console.error('Search error: ', err));
  }

  render() {
    return (
      <nav id="header">
        <button onClick={ () => this.getSaved() }>
          My saved heroes
        </button>
        <div>
          <input
            type="text"
            onChange={ e => this.handleChange(e) }
            value={ this.state.text }
            />
          <button onClick={ (e) => this.search(e) }>
            Search
          </button>
        </div>
      </nav>
    )
  }
}