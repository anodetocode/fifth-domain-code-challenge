import React from 'react';
import { saveHero } from '../services/api-client';

export default class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props.hero,
      isEditing: false,
      isSaving: false
    }
  }

  handleChangeEdit() {
    this.setState({ isEditing: true });
  }

  handleChangeSave() {
    this.setState({isSaving: true, isEditing: false});
    const {isEditing, isSaving, ...hero} = this.state;
    saveHero(hero).then(() => {
      this.setState({ isSaving: false });
    })
  }

  handleChangeStats(e, field) {
    let value = e.target.value;
    if (value > 100) v = 100;
    if (value < 0) v = 0;
    this.setState({
      powerstats: {...this.state.powerstats, [field]: value}
    });
  }

  render() {
    const {id, name, powerstats, image} = this.state;
    return (
      <li key={id}>
        <img src={image} />
        <h2>{name}</h2>
        <ul>
          {Object.entries(powerstats).map(([k,v], index) => {
            if (v === 'null') v = 0
            return (
              <li key={id + index}>
                <label>{k}: </label>
                {
                  this.state.isEditing ?
                  (<input onChange={(e) => this.handleChangeStats(e, k)} type="number" value={v} />) :
                  <span>{v}</span>
                }
              </li>
            )
          })}
        </ul>
        { this.state.isEditing ?
          <button onClick={() => this.handleChangeSave()}>Save</button> :
          <button onClick={() => this.handleChangeEdit()}>Edit</button>
        }
      </li>
    );
  }
}