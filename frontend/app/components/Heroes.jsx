import React from 'react';
import Card from './Card';

export default class Heroes extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const heroes = this.props.heroes;
    const isSearch = this.props.isSearch;
    if (!heroes) return <div>Loading...</div>;

    return (
      <div id="container">
        <h1>{isSearch ? 'Search results' : 'Saved Heroes'}</h1>
        <ul id="card-container">
          { heroes.length ?
            heroes.map((hero, index) => <Card key={hero.id + index} hero={hero}/> ) :
            <div>No results...</div>
          }
        </ul>
      </div>
    );
  }
}