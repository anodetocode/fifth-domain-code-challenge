import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Header from './components/Header';
import Heroes from './components/Heroes';

class App extends React.Component {
  constructor() {
    super();
    this.handleChangeHeroes = this.handleChangeHeroes.bind(this)
    this.state = {
      heroes: [],
      isSearch: false
    }
  }

  handleChangeHeroes(heroes) {
    this.setState({ heroes: heroes });
  }

  handleChangeSearch(isSearch) {
    this.setState({ isSearch });
  }

  render() {
    return (
      <React.Fragment>
        <Header
          onChangeSearch={ value => this.handleChangeSearch(value) }
          onChangeHeroes={ value => this.handleChangeHeroes(value) }
        ></Header>
        <Heroes
          isSearch={this.state.isSearch}
          heroes={this.state.heroes}
        ></Heroes>
      </React.Fragment>
    );
  }
}

ReactDOM.render(
  <App></App>,
  document.getElementById('app')
);
