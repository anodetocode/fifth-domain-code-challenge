const apiUrl = process.env.BACKEND_API || `http://vps-b48bd885.vps.ovh.ca:8080`;

export function searchHero(name) {
  return fetch(`${apiUrl}/search`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ name })
  })
    .then(res => res.json())
    .catch(err => console.error('#searchHero: ', err));
}

export function getSavedHeroes() {
  return fetch(`${apiUrl}/heroes`)
    .then(res => res.json())
    .catch(console.log);
}

export function saveHero(hero) {
  return fetch(`${apiUrl}/heroes/${hero.id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(hero)
  })
  .catch(console.log);
}
